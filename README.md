# VIZ Authors Collaboration

**CZ:** Semestrální projekt z předmětu VIZ (B4M39VIZ - FEL ČVUT) pro letní semestr 2019.

**EN:** Semestral project for VIZ subject (B4M39VIZ - FEL CTU) in summer semester 2019.

**Author:** Bc. Jordan Moravenov

More information about the project (only CZ): `/docs/STAR-final-report.pdf`


## Setup PyCharm project
- install required node modules (mathjs)
- setup file watcher for `/app/style.sass`
### File watcher - SASS
- install sass
- install nodejs
- make sure `/usr/bin/node` exists. if not make a symlink
- install nodejs plugin into pycharm
- add watcher (pychram will suggest on opening sass file) and link program to sass executable
