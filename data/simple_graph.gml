graph
[
  directed 0
  node
  [
    id 0
    label "A"
  ]
  node
  [
    id 1
    label "B"
  ]
  node
  [
    id 2
    label "C"
  ]
  edge
  [
    source 1
    target 0
    value 2.5
  ]
  edge
  [
    source 1
    target 2
    value 0.25
  ]
  edge
  [
    source 0
    target 2
    value 0.25
  ]
]
