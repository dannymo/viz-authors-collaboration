graph
[
  directed 0
  node
  [
    id 0
    label "A"
  ]
  node
  [
    id 1
    label "B"
  ]
  node
  [
    id 2
    label "C"
  ]
  node
  [
    id 3
    label "AA"
  ]
  node
  [
    id 4
    label "BB"
  ]
  node
  [
    id 5
    label "CC"
  ]
  edge
  [
    source 1
    target 0
    value 2.5
  ]
  edge
  [
    source 1
    target 2
    value 0.25
  ]
  edge
  [
    source 0
    target 2
    value 0.25
  ]
    edge
  [
    source 4
    target 3
    value 2.5
  ]
  edge
  [
    source 4
    target 5
    value 0.25
  ]
  edge
  [
    source 3
    target 5
    value 0.25
  ]
  edge
  [
    source 0
    target 3
    value 0.25
  ]
]
