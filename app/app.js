(function(globals) {
    const CANVAS_ELEMENT_WIDTH = 600;
    const CANVAS_ELEMENT_HEIGHT = 600;


    function loadGraph(event) {
        let oldGraph = globals.g;
        stopKamadaKawai();
        globals.stepIndex = 1;
        let file = document.querySelector('input[type=file]').files[0];
        let reader = new FileReader();
        reader.onload = function (event) {
            let graph = parseGML(event.target.result);
            let canvas = document.querySelector("#graph canvas");
            graph.initVertexPositions(canvas);
            graph.draw();
            if (oldGraph !== null) {
                graph.painter = oldGraph.painter;
            }
            globals.g = graph;
            resetDetails();
            console.log("Loaded graph saved under global variable 'g'");
        };
        reader.readAsText(file);
    }


    function canvasClickHandler(event) {
        if (!globals.ctrlPressed && globals.g !== null) {
            let x = 2 * (event.pageX - event.target.offsetLeft),
                y = 2 * (event.pageY - event.target.offsetTop);

            // Collision detection between `ed offset and element.
            let v = globals.g.getVertexOnCoords(
                (x / globals.g.painter.scale + globals.g.painter.originx),
                (y / globals.g.painter.scale + globals.g.painter.originy),
            );
            globals.g.draw();

            if (v === null) {
                resetDetails();
            } else {
                updateDetails(v);
            }
        }
    }


    function mouseMoveHandler(event) {
        if (globals.g === null) {
            return;
        }
        if (!globals.lastClick) {
            globals.lastClick = {
                x: event.clientX,
                y: event.clientY,
            };
            return;
        }
        if (globals.ctrlPressed && globals.mouseDown) {
            globals.g.translate(
                2 * (event.clientX - globals.lastClick.x),
                2 * (event.clientY - globals.lastClick.y),
            );
        }
        globals.lastClick = {
            x: event.clientX,
            y: event.clientY,
        };
    }


    function mouseDownHandler(event) {
        globals.mouseDown = true;
        if (globals.g !== null) {
            document.querySelector("#graph canvas").classList.add("mousedown");
        }
    }

    function mouseUpHandler(event) {
        globals.mouseDown = false;
        document.querySelector("#graph canvas").classList.remove("mousedown");
    }


    function keydownHandler(event) {
        if (event.key === "Control") {
            globals.ctrlPressed = true;
            if (globals.g !== null) {
                document.querySelector("#graph canvas").classList.add("ctrl");
            }
        }
        if (event.key === "i") {
            if (globals.g !== null) {
                initKamadaKawai(globals.g);
            }
        }
        if (event.key === "k") {
            nextStepKamadaKawai();
        }
    }


    function keyupHandler(event) {
        if (event.key === "Control") {
            globals.ctrlPressed = false;
            document.querySelector("#graph canvas").classList.remove("ctrl");
        }
    }


    function resetDetails() {
        let details = document.querySelector("#details");
        details.innerHTML = `<h1 class="none">No node selected</h1>`;
    }


    function updateDetails(v) {
        let details = document.querySelector("#details");
        collaborators = [];
        let neighbour = v.next;
        while (neighbour !== null) {
            let vertex = g.getVertex(neighbour.index);
            collaborators.push({
                "name": vertex.label,
                "collaboration": neighbour.weight,
                "index": vertex.index,
            });
            neighbour = neighbour.next;
        }

        details.innerHTML = `
            <h1>${v.label} <span style="color: gray">(#${v.index})</span></h1>
            <table>
                <tr><th>Energy</th><td>${v.energy}</td></tr>
            </table>
            <h2>Collaborators</h2>
            <table>
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Collaboration level</th>
                </thead>
                ${renderCollaboratorsList(collaborators).join("\n")}
            </table>
`;
    }


    globals.updateDetailsOfIndex = function(i) {
        let graph = globals.g;
        updateDetails(graph.getVertex(i));
        graph.vertices.map(v => v.clearSelected());
        graph.getVertex(i).setSelected();
        graph.draw();
    };


    function renderCollaboratorsList(collaborators) {
        /**
         * Input: List of collaborators (i. e. objects with namupdateDetailsOfIndex es and collaboration value)
         * Output: list of HTML list items (i. e. <li>...</li>)
         */
        return collaborators.map(o => `
        <tr>
            <td>#${o.index}</td>
            <td><a href="javascript:updateDetailsOfIndex(${o.index})">${o.name}</a></td>
            <td>${o.collaboration}</td>
        </tr>
    `)

    }


    function canvasScrollHandler(event) {
        globals.g.painter.zoom(event);
        globals.g.draw();
    }


    function resetView() {
        globals.g.painter.resetTransforms();
        globals.g.draw();
    }


    function changeNodeSize(event) {
        globals.g.painter.nodeSize = event.target.value;
        globals.g.draw();
    }


    function addListener(elementId, type, listener) {
        document.getElementById(elementId).addEventListener(type, listener);
    }


    function main() {
        // Check for the various File API support.
        if (globals.File && globals.FileReader && globals.FileList && globals.Blob) {
            /**
             * Graph saved to global variable.
             */
            globals.g = null;
            let canvas = document.querySelector("#graph canvas");
            canvas.width = 2 * CANVAS_ELEMENT_WIDTH;
            canvas.height = 2 * CANVAS_ELEMENT_HEIGHT;
            canvas.addEventListener('click', canvasClickHandler, false);

            canvas.addEventListener("wheel", canvasScrollHandler);
            document.body.addEventListener('mousedown', mouseDownHandler);
            document.body.addEventListener('mouseup', mouseUpHandler);
            document.body.addEventListener('mousemove', mouseMoveHandler);
            document.body.addEventListener("keydown", keydownHandler);

            document.body.addEventListener("keyup", keyupHandler);
            addListener("inputFile", "change", loadGraph);
            addListener("load", "click", loadGraph);
            addListener("start", "click", () => startKamadaKawai(globals.g));
            addListener("init", "click", () => initKamadaKawai(globals.g));
            addListener("step", "click", nextStepKamadaKawai);
            addListener("stop", "click", stopKamadaKawai);
            addListener("resetView", "click", resetView);
            addListener("nodeSize", "input", changeNodeSize);
        } else {
            alert("Your browser is too old to support HTML5 File API");
        }
    }

    document.addEventListener("DOMContentLoaded", main);
})(window);
