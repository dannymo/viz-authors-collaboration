const EDGE_LENGTH = 50;
const STIFFNESS = 1;
const EPSILON = 10;
const DRAW_ONCE_PER = 10;


function getShortestPathsMatrixNaive(graph) {
    const matrix = [];
    for (let i = 0; i < graph.length; i++) {
        matrix[i] = new Array(graph.length).fill(-1);
    }
    let iteration_id = 0;
    for (let vertex of graph.vertices) {
        let queue = [];
        vertex.visited = iteration_id;
        vertex.d = 0;
        matrix[vertex.index][vertex.index] = 0;
        queue.push(vertex);
        while (queue.length > 0) {
            let v = queue.shift();
            let neighbour = v.next;
            while (neighbour !== null) {
                let w = graph.getVertex(neighbour.index);
                if (w.visited !== iteration_id) {
                    w.d = v.d + 1;
                    matrix[vertex.index][w.index] = w.d;
                    w.visited = iteration_id;
                    queue.push(w);
                }
                neighbour = neighbour.next;
            }
        }
        iteration_id++;
    }
    return matrix;
}

function printm(matrix) {
    for(let line of matrix) {
        console.log(line.map(x => x.toString().padStart(3, " ")).join(" "));
    }
    console.log("");
}

function getVerticesWithEnergy(graph, ks, ls) {
    /**
     * computes energy for every vertex and returns vertex list sorted (highest energy first).
     */
    let max_energy = -1;
    let max_energy_vertex = null;
    let iteration_id = 0;
    let vertices = [];
    for (let vertex of graph.vertices) {
        if (vertex.computeEnergy(ks, ls) > 0)
            vertices.push(vertex);
        if (vertex.energy > max_energy) {
            max_energy = vertex.energy;
            max_energy_vertex = vertex;
        }
        iteration_id++;
    }
    return vertices.sort((x, y) => y.energy - x.energy);
}


function distance(v1, v2) {
    return Math.sqrt(
        Math.pow(v1.x - v2.x, 2)
        + Math.pow(v1.y - v2.y, 2)
    );
}


function energyOfSpring(v1, v2, ks, ls) {
    // TODO: see energy equation (9) in the article
    const k = ks[v1.index][v2.index];
    const l = ls[v1.index][v2.index];
    return (k / 2) * Math.pow(distance(v1, v2) - l, 2)
    // return (k / 2) * (Math.pow(v1.x - v2.x, 2) + Math.pow(v1.y - v2.y, 2) + l*l - 2*l*distance(v1,v2));
}


function getDeltaXYToEnergyMinimum(v, ks) {
    /**
     * For given vertex changes x and y to move it to place where it has lowest energy.
     *
     * We want to compute dx, dy.
     * deriv_x_sq * dx + deriv_x_y  * dy = - deriv_x
     * deriv_y_x  * dx + deriv_y_sq * dy = - deriv_y
     */
    if (v.next === null) return [0, 0];
    let deriv_x = 0;
    let deriv_y = 0;
    let deriv_x_sq = 0;
    let deriv_y_sq = 0;
    let deriv_x_y = 0;
    let deriv_y_x = 0;
    for (let w of v.graph.vertices) {
        if (v.index === w.index) continue;
        if (ds[v.index][w.index] < 0) continue;
        let k = ks[v.index][w.index];
        let l = ls[v.index][w.index];
        let dist_vw = distance(v, w);
        deriv_x += k * ((v.x - w.x) - ((l * (v.x - w.x)) / dist_vw));
        deriv_y += k * ((v.y - w.y) - ((l * (v.y - w.y)) / dist_vw));
        deriv_x_sq += k * (1 - ((l * Math.pow(v.y - w.y, 2)) / Math.pow(dist_vw, 3)));
        deriv_y_sq += k * (1 - ((l * Math.pow(v.x - w.x, 2)) / Math.pow(dist_vw, 3)));
        deriv_x_y += k * ((l * (v.x - w.x) * (v.y - w.y)) / Math.pow(dist_vw, 3));
    }
    deriv_y_x = deriv_x_y;

    matrix = [
        [deriv_x_sq, deriv_x_y],
        [deriv_y_x, deriv_y_sq],
    ];
    if (math.det(matrix) === 0) {
        return [0, 0];
    }
    matrix_inv = math.inv(matrix);
    vector = [-deriv_x, -deriv_y];
    return math.multiply(matrix_inv, vector);
}


function moveVertexToEnergyMinimum(v, ks) {
    dx_dy = getDeltaXYToEnergyMinimum(v, ks);
    v.x += dx_dy[0];
    v.y += dx_dy[1];
}



/**
 * Runs Kamada Kawai algorithm from the current graph state as long as
 * the global variable "kamadaKawaiRunning" is set to true.
 *
 * @param graph {Graph}
 */
function startKamadaKawai(graph) {
    if (window.kamadaKawaiRunning) {
        console.warn("KamadaKawai already running.");
        return;
    }
    initKamadaKawai(graph);
    window.kamadaKawaiStepId = 0;
    let process = function() {
        if (kamadaKawaiRunning === false) {
            console.log("KamadaKawai stopped");
            graph.draw();
            return;
        }

        let most_energetic_vertices = getVerticesWithEnergy(g, ks, ls);
        stepKamadaKawai(most_energetic_vertices);
        if (kamadaKawaiStepId % DRAW_ONCE_PER === 0) {
            graph.draw();
        }
        kamadaKawaiStepId++;
        if (most_energetic_vertices[0].energy > EPSILON) {
            setTimeout(process, 0);
        }
        else {
            console.log("KamadaKawai Finished.");
            graph.draw();
        }
    };
    window.kamadaKawaiRunning = true;
    console.log("KamadaKawai running...");
    process();
}


function initKamadaKawai(graph) {
    // TODO: get rid of global ds, ls, ks (maybe create and class and get these where necessary via "this")
    window.ds = getShortestPathsMatrixNaive(graph);
    window.ls = ds.map(x => x.map(y => y * EDGE_LENGTH));
    window.ks = ds.map(x => x.map(y => STIFFNESS / (y*y)));
    console.log("init DONE");
}


function almostEqual(a, b) {
    return Math.abs(a - b) < 10.0001;
}


function stepKamadaKawai(vertices) {
    /**
     * Input: Vertices ordered from highest energy to lowest.
     */
    for (let v of vertices) {
        let last_energy = v.energy;
        while (true) {
            moveVertexToEnergyMinimum(v, ks);
            if (almostEqual(v.energy, last_energy)) break;
            if (v.energy > last_energy) break;
            else last_energy = v.energy;
        }
    }
    window.stepIndex++;
}


function nextStepKamadaKawai() {
    let stepStr = `step ${stepIndex}`;
    console.time(stepStr);
    const STEPS_BURST_COUNT = 1;
    let i = 0;
    let process = function() {
        if (i >= STEPS_BURST_COUNT) {
            g.draw();
            return;
        }
        let most_energetic_vertices = getVerticesWithEnergy(g, ks, ls);
        stepKamadaKawai(most_energetic_vertices);
        setTimeout(process, 0);
        i++;
    };
    process();
    console.timeEnd(stepStr);
}


function stopKamadaKawai() {
    window.kamadaKawaiRunning = false;
}
