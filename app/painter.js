/**
 * Painter creates layer of abstraction above
 * the canvas and its context and provides more drawing methods.
 */
class Painter {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");
        this.nodeSize = document.querySelector("input[type=range]").value;

        // zoom properties
        this.scale = 1;
        this.originx = 0;
        this.originy = 0;
        this.width = this.canvas.width;
        this.height = this.canvas.height;
        this.visibleWidth = this.canvas.width;
        this.visibleHeight = this.canvas.height;
    }
    drawCircle(color, x, y) {
        this.ctx.fillStyle = color;
        this.ctx.beginPath();
        this.ctx.arc(x, y, this.nodeSize, 0, 2 * Math.PI);
        this.ctx.stroke();
        this.ctx.fill();
    }
    drawLine(color, x1, y1, x2, y2, width=1) {
        this.ctx.strokeStyle = color;
        this.ctx.lineWidth = width;
        this.ctx.beginPath();
        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);
        this.ctx.stroke();
    }
    clear() {
        this.ctx.clearRect(this.originx, this.originy, this.visibleWidth, this.visibleHeight);
    }

    translate(dx, dy) {
        this.ctx.translate(dx/this.scale, dy/this.scale);
        this.originx -= dx/this.scale;
        this.originy -= dy/this.scale;
    }

    /**
     * Source: https://stackoverflow.com/questions/2916081/zoom-in-on-a-point-using-scale-and-translate#answer-3151987
     *
     * @param event ... mouse wheel event
     */
    zoom(event) {
        const ZOOM_INTENSITY = 0.2;

        let canvas = document.querySelector("#graph canvas");
        event.preventDefault();
        // Get mouse offset.
        let mousex = 2 * (event.clientX - canvas.offsetLeft);
        let mousey = 2 * (event.clientY - canvas.offsetTop);
        // Normalize wheel to +1 or -1.
        let wheel = event.deltaY < 0 ? 1 : -1;

        // Compute zoom factor.
        let zoom = Math.exp(wheel * ZOOM_INTENSITY);

        // Translate so the visible origin is at the context's origin.
        this.ctx.translate(this.originx, this.originy);

        // Compute the new visible origin. Originally the mouse is at a
        // distance mouse/scale from the corner, we want the point under
        // the mouse to remain in the same place after the zoom, but this
        // is at mouse/new_scale away from the corner. Therefore we need to
        // shift the origin (coordinates of the corner) to account for this.
        this.originx -= mousex/(this.scale*zoom) - mousex/this.scale;
        this.originy -= mousey/(this.scale*zoom) - mousey/this.scale;

        // Scale it (centered around the origin due to the translate above).
        this.ctx.scale(zoom, zoom);
        // Offset the visible origin to it's proper position.
        this.ctx.translate(-this.originx, -this.originy);

        // Update scale and others.
        this.scale *= zoom;
        this.visibleWidth = this.width / this.scale;
        this.visibleHeight = this.height / this.scale;
    //    TODO: bug - on zoom, mouse cursor doesn't stay at the same position
    }

    resetTransforms() {
        /**
         * Resets transforms applied by zoom and translate methods.
         */
        this.ctx.translate(this.originx, this.originy);
        this.originx = 0;
        this.originy = 0;

        this.ctx.scale(1/this.scale, 1/this.scale);
        this.scale = 1;
        this.visibleWidth = this.width;
        this.visibleHeight = this.height;
    }
}

/**
 * Painter singleton.
 *
 * When a new graph is loaded, new Graph instance is created,
 * but the information about the viewport zoom and translation remains.
 */
window.painter = null;
function getPainter(canvas) {
    if (window.painter === null) {
        window.painter = new Painter(canvas);
    }
    return window.painter;
}
