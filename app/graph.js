/**
 * Linked graph.
 */
class Graph {
    constructor(painter) {
        this._vertices = [];
        this._painter = painter;
    }
    get length() {
        return this._vertices.length;
    }
    get vertices() {
        return this._vertices
    }
    get painter() {
        return this._painter;
    }
    set painter(newPainter) {
        this._painter = newPainter;
    }
    addVertex(vertex) {
        this._vertices.push(vertex);
    }
    getVertex(index) {
        return this._vertices[index];
    }

    /**
     * Link drawing canvas to the graph and position the vertices
     * on a circle according to the Kamada-Kawai article.
     *
     * @param canvas ... canvas html element
     */
    initVertexPositions(canvas) {
        let r = 0.9 * Math.min(canvas.width, canvas.height) / 2;
        let sx = canvas.width / 2;
        let sy = canvas.height / 2;
        let vertexCount = this._vertices.length;
        let fi = 2 * Math.PI / vertexCount;
        for (let i = 0; i < vertexCount; i++) {
            let angle = i * fi;
            let v = this.getVertex(i);
            v.x = r * Math.cos(angle) + sx;
            v.y = r * Math.sin(angle) + sy;
        }
    }
    clear() {
        this._painter.clear();
    }
    draw() {
        if (this._painter === null) {
            console.error(
                "Graph has no context to draw. Have you already called initVertexPositions?"
            );
        }
        this.clear();
        for (let v of this._vertices) {
            v.draw(this._painter);
        }
        for (let v of this._vertices) {
            if (v.selected) {
                v.draw(this._painter);
                break;
            }
        }
    }

    getVertexOnCoords(x, y) {
        let found_v = null;
        for (let v of this._vertices) {
            if (
                found_v === null
                && (
                    Math.pow(v.x - x, 2) + Math.pow(v.y - y, 2)
                    <=
                    Math.pow(this._painter.nodeSize, 2)
                )
            ) {
                v.setSelected();
                found_v = v;
            }
            else {
                v.clearSelected();
            }
        }
        return found_v;
    }

    translate(x, y) {
        this._painter.translate(x, y);
        this.draw();
    }
}

/**
 * Node of a Graph
 */
class Node {
    constructor(graph) {
        this._graph = graph;
        this._index = null;
        this._label = null;
        this._next = null;
        this._x = 0;
        this._y = 0;
        this._color = "#000000";
        this._colorSelected = "#FF0000";

        // traversing related tmp attributes
        this.d = -1;
        this.visited = -1;
        this.energy = -1;
    }

    /* Getters */
    get graph() {
        return this._graph;
    }
    get index() {
        return this._index;
    }
    get label() {
        return this._label
    }
    get next() {
        return this._next;
    }
    get x() {
        return this._x;
    }
    get y() {
        return this._y;
    }
    get selected() {
        return this._selected;
    }

    /* Setters */
    set index(index) {
        this._index = index;
    }
    set label(label) {
        this._label = label;
    }
    set x(value) {
        this._x = value;
    }
    set y(value) {
        this._y = value;
    }
    setSelected() {
        this._selected = true;
    }
    clearSelected() {
        this._selected = false;
    }

    /* Methods */
    addNext(weakNode) {
        if (this._next === null) {
            this._next = weakNode;
        }
        // TODO: iterate instead of recursion
        else {
            this._next.addNext(weakNode);
        }
    }

    draw(painter) {
        let color = (this._selected) ? this._colorSelected : this._color;
        painter.drawCircle(color, this._x, this._y);
        let neighbour = this._next;
        while (neighbour !== null) {
            painter.drawLine(
                color,
                this._x, this._y,
                this._graph.getVertex(neighbour.index).x,
                this._graph.getVertex(neighbour.index).y,
                (this._selected) ? 5 : 1
            );
            neighbour = neighbour.next;
        }
    }

    // TODO: This is more related to kamada_kawai, not Graph. move it
    computeEnergy(ks, ls) {
        this.energy = 0;
        if (this.next === null) return this.energy;
        for (let w of this._graph.vertices) {
            if (this.index === w.index) continue;
            if (ls[this.index][w.index] < 0) continue;
            this.energy += energyOfSpring(this, w, ks, ls);
        }
        return this.energy;
    }
}

/**
 * Item of linked list representing a Node neighbours.
 * It contains edge information.
 */
class WeakNode {
    constructor(index, weight) {
        this.index = index;
        this.weight = weight;
        this.next = null;
    }
    addNext(weakNode) {
        if (this.next === null) {
            this.next = weakNode;
        }
        // TODO: iterate instead of recursion
        else {
            this.next.addNext(weakNode);
        }
    }
}


function strip(str) {
    return str.replace(/^\s+|\s+$/g, '');
}


/**
 * Parse graph from text in GML format and returns Graph instance.
 *
 * Constrains on GML formatting:
 * - symbols "[" and "]" must be on separate line
 * - keywords "node", "edge" must be on separate line
 * - keywords "source", "target", "value" must be on separate line and
 *   must be followed by one space and it's value
 *
 * @param text
 * @returns {Graph}
 */
function parseGML(text) {
    let lines = text.match(/[^\r\n]+/g);
    let itemType = null;
    let item = null;
    let graph = new Graph(
        getPainter(document.querySelector("#graph canvas"))
    );
    for (let l of lines) {
        let line = strip(l);
        if (line === "node") {
            itemType = "node";
            item = new Node(graph);
            continue;
        }
        if (line === "edge") {
            itemType = "edge";
            item = {};
            continue;
        }
        if (line === "]" && item) {
            if (itemType === "node") {
                graph.addVertex(item);
                item = null;
                itemType = null;
            }
            else if (itemType === "edge") {
                graph.getVertex(item.source).addNext(
                    new WeakNode(item.target, item.value)
                );
                graph.getVertex(item.target).addNext(
                    new WeakNode(item.source, item.value)
                );
                item = null;
                itemType = null;
            }
            continue;
        }

        if (itemType === "node") {
            indexRegex = /^index ([0-9]+)$/g;
            indexMatch = indexRegex.exec(line);
            if (indexMatch) {
                item.index = indexMatch[1];
            }

            labelRegex = /^label "(.+)"$/g;
            labelMatch = labelRegex.exec(line);
            if (labelMatch) {
                item.label = labelMatch[1];
            }
        }
        if (itemType === "edge") {
            sourceRegex = /^source ([0-9]+)$/g;
            sourceMatch = sourceRegex.exec(line);
            if (sourceMatch) {
                item.source = sourceMatch[1];
            }

            targetRegex = /^target ([0-9]+)$/g;
            targetMatch = targetRegex.exec(line);
            if (targetMatch) {
                item.target = targetMatch[1];
            }
            valueRegex = /^value ([0-9 .]+)$/g;
            valueMatch = valueRegex.exec(line);
            if (valueMatch) {
                item.value = valueMatch[1];
            }
        }
    }
    for(let i = 0; i < graph.length; i++) {
        graph.getVertex(i).index = i;
    }
    return graph;
}
