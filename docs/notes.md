Zadání
======
- graf máme považovat za neorientovaný
- ohodnocení hran reprezentuje abstraktní míru spolupráce
- nemáme řešit rozmisťování popisků (to je NP úplná úloha)
    - namísto toho např. tzv. "overview detail" - kliknutí na vrchol -> vypsání informací bokem

Do 9. týdne
-----------
- naučit se z článků, jaké existují metody vizualizace grafů
- posoudit jejich vhodnost pro naši úlohu (s tím nám může pomoci gephi)
- přijít s návrhem, jak budeme naši úlohu řešit (následná implementace se od tohoto návrhu může lišit)

Do konce semestru
-----------------
- implementovat jednu vybranou metodu
- vizualizace + další okénka pro detail
- 2 možné přístupy
    1) dlouho trvající rendering, který vygeneruje dokonalý obrázek
    2) rychlé nedokonalé zobrazení s možností interakce
        - uživatel si interakcí vylepší zobrazení
        - uivatel si ke splnění úkolu pomůže filtrováním